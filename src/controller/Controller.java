package controller;

import java.io.File;

import api.IDivvyTripsManager;
import model.data_structures.LinkedList;
import model.logic.DivvyTripsManager;
import model.vo.VOTrip;

public class Controller {
	public final static String rutaGeneral="."+File.separator+"data"+File.separator;
	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IDivvyTripsManager  manager = new DivvyTripsManager();
	
	public static void loadStations() {
		manager.loadStations(rutaGeneral+"Divvy_Stations_2017_Q3Q4.csv");
	}

	public static void loadTrips() {
		manager.loadTrips(rutaGeneral+"Divvy_Trips_2017_Q3.csv");
	}
		
	public static LinkedList <String> getLastNStations (int bicycleId, int n) {
		return manager.getLastNStations(bicycleId, n);
	}
	
	public static VOTrip customerNumberN (int stationID, int n) {
		return manager.customerNumberN(stationID, n);
	}
}
