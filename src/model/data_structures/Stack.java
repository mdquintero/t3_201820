/**
 * Autores: Mateo David Quintero Reyes, Daniel Armando Babativa Aparicio
 */
package model.data_structures;

import java.util.Iterator;

/**
 * Implementacion de una pila generica
 * Esta implementacion se hizo en base al material trabajado en clase
 * @param <T> Objeto generico
 */
public class Stack<T> implements IStack<T> {

	//ATRIBUTOS
	private int size;
	private Node<T> top, bottom;



	//CONSTRUCTOR(ES)
	public Stack(){
		top=null;
		size=0;
		bottom=null;
	}


	//M�TODOS
	@Override
	public Iterator<T> iterator(){
		return new ListIterator();
	}

	public Node<T> getBottom(){
		return bottom;
	}

	private class ListIterator implements Iterator<T> 
	{
		private Node<T> current = top;


		public boolean hasNext()
		{ 
			return current != null;               	
		}

		public T next()
		{
			if (!hasNext()) 
				return null;


			T item = (T) current.getItem();
			current = current.getNext(); 
			return item;
		}
	}

	@Override
	public boolean isEmpty() {
		return (size==0);
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public void push(T t) {
		if(top==null){
			top=new Node<T>(t, null, null);
			bottom=new Node<T>(t, null, null);
			
		}
		
			Node<T> newTop = new Node<T>(t, top, null);;
			top.setPrev(newTop);
			newTop.setNextNode(top);
			top=newTop;
			size++;
		

	}

	@Override
	public T pop() {
		if(top==null){
			System.out.println("No hay elementos en la pila");
			return null;
		}
		else if(top==bottom){
			T item = top.getItem();
			top=null;
			bottom=null;
			size--;
			return item;
		}
		T item = top.getItem();
		Node<T> newTop = top.getNext();
		newTop.setPrev(null);
		top.setNextNode(null);
		top=newTop;
		size--;
		return item;
	}



}
