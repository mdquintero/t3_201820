package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Calendar;

import api.IDivvyTripsManager;
import model.data_structures.LinkedList;
import model.data_structures.Node;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.Station;
import model.vo.VOTrip;

public class DivvyTripsManager implements IDivvyTripsManager {

	private Stack<VOTrip> stackTrips= new Stack<VOTrip>();

	private Queue<VOTrip> queueTrips = new Queue<VOTrip>();

	private LinkedList<Station> listaEncadenadaStations= new LinkedList<Station>();


	public void loadStations (String stationsFile) {
		try{
			FileReader fr = new FileReader(new File(stationsFile));
			BufferedReader br = new BufferedReader(fr);

			String line = br.readLine();
			//Se avanza de nuevo porque la l�nea anterior no contiene datos
			line = br.readLine();

			while(line!=null)
			{


				String[] lineArray = line.split(",");

				Station s = new Station(lineArray[0], lineArray[1], lineArray[2], Double.parseDouble(lineArray[3]), Double.parseDouble(lineArray[4]), 
						Integer.parseInt(lineArray[5]), Calendar.getInstance());


				listaEncadenadaStations.add(s);

				line = br.readLine();
			}

			br.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}

	public void loadTrips (String tripsFile) {



		try{
			FileReader fr = new FileReader(new File(tripsFile));
			BufferedReader br = new BufferedReader(fr);

			VOTrip t=null;



			String line = br.readLine();
			//Se avanza de nuevo porque la l�nea anterior no contiene datos
			line = br.readLine();

			while(line!=null)
			{


				String[] lineArray = line.split(",");



				if(lineArray[10].equals("\"\"")){

					t = new VOTrip(Integer.parseInt(lineArray[0].replace("\"","")), lineArray[1], lineArray[2], Integer.parseInt(lineArray[3].replace("\"","")), 
							Integer.parseInt(lineArray[4].replace("\"","")), Integer.parseInt(lineArray[5].replace("\"","")), lineArray[6], Integer.parseInt(lineArray[7].replace("\"","")), lineArray[8], lineArray[9]);

				}
				else{
					t = new VOTrip(Integer.parseInt((lineArray[0].replace("\"",""))), lineArray[1], lineArray[2], Integer.parseInt(lineArray[3].replace("\"","")), 
							Integer.parseInt(lineArray[4].replace("\"","")), Integer.parseInt(lineArray[5].replace("\"","")), lineArray[6], Integer.parseInt(lineArray[7].replace("\"","")), lineArray[8], lineArray[9],
							lineArray[10], Integer.parseInt(lineArray[11].replace("\"","")));

				}
				stackTrips.push(t);
				queueTrips.enqueue(t);




				line = br.readLine();


			}

			br.close();

		}
		catch(Exception e){
			e.printStackTrace();
		}


	}

	@Override
	public LinkedList <String> getLastNStations (int bicycleId, int n) {
		LinkedList <String> stationsList = new LinkedList<String>();
		Queue<VOTrip> temp = this.queueTrips;
		Node<VOTrip> current = temp.getLast();
		int counter=n;
		

		while( current!=null && counter!=0)
		{
			
			if(current.getItem().getBikeId()==bicycleId){
				counter--;
				stationsList.add(current.getItem().getFromStation());
			}
			current=current.getPrev();
		}
		return stationsList;

	}

	@Override
	public VOTrip customerNumberN (int stationID, int n) {
		Stack<VOTrip> temp = this.stackTrips;
		VOTrip r = null;
		Node<VOTrip> current=temp.getBottom();
		int counter = n;
		while((current!=null && counter !=0))
		{
			if(current.getItem().getIdToStation()==stationID)
			{
				counter--;
				if(counter == 0)
				{
					r = current.getItem();
				}
			}
			current=current.getPrev();
		}
		return r;
	}	


}
