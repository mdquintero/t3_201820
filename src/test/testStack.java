package test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.Stack;

public class testStack {


	//ATRIBUTOS
	private Stack<Integer> stackIntegers;


	private Stack<String> stackStrings;


	//METODOS
	private void setupEscenarioIntegers(){

		stackIntegers = new Stack<Integer>();

		stackIntegers.push(new Integer(1));
		stackIntegers.push(new Integer(2));
		stackIntegers.push(new Integer(3));
		stackIntegers.push(new Integer(4));
		stackIntegers.push(new Integer(5));
		stackIntegers.push(new Integer(6));
		stackIntegers.push(new Integer(7));

	}

	private void setupEscenarioStrings(){

		stackStrings = new Stack<String>();

		stackStrings.push("aaa");
		stackStrings.push("bbb");
		stackStrings.push("ccc");
		stackStrings.push("ddd");
		stackStrings.push("fff");
		stackStrings.push("ggg");
		stackStrings.push("hhh");

	}

	
	@Test
	public void testPush(){
		setupEscenarioIntegers();
		setupEscenarioStrings();


		assertEquals( "El metodo push no agrego todos los elementos a la lista", 7, stackIntegers.size() );
		assertEquals( "El metodo push no agrego todos los elementos a la lista", 7, stackStrings.size() );

	}

	

	@Test
	public void testPop(){
		setupEscenarioIntegers();
		setupEscenarioStrings();


		stackStrings.pop();
		stackIntegers.pop();

		assertEquals( "El metodo pop no elimino el elemento de la lista", 6, stackIntegers.size() );
		assertEquals( "El metodo pop no elimino el elemento de la lista", 6, stackStrings.size() );
	}

	@Test
	public void testIsEmpty(){
		stackIntegers = new Stack<Integer>();
		stackStrings = new Stack<String>();

		assertTrue( "La lista deberia estar vacia", stackIntegers.isEmpty() );
		assertTrue( "La lista deberia estar vacia", stackStrings.isEmpty() );
	}

	
	
	
}
